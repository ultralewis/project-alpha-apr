from django.forms import ModelForm
from django import forms
from tasks.models import Task


class DateInput(forms.DateInput):
    input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )
        widgets = {
            "start_date": DateInput(),
            "due_date": DateInput(),
        }
